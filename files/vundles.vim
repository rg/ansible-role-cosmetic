" ========================================
" Vim plugin using Vundle
" ========================================
"
" Start with a checkout
" git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
"
" Then copy this file in ~/.vim/bundle/vundles.vim
"
" Finally install the plugins
" Start vim then :PluginInstall
" Or command vim --noplugin -u ~/.vim/bundle/vundles.vim -N "+set hidden" "+syntax on" +PluginClean! +PluginInstall +qall

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Bundle "scrooloose/nerdtree.git"
Bundle "tpope/vim-repeat.git"
Bundle "tpope/vim-surround.git"
Bundle "tpope/vim-liquid.git"
Bundle "skwp/vim-colors-solarized"
Bundle "tpope/vim-git"
Bundle "skwp/YankRing.vim"

" Bundle "vim-ruby/vim-ruby.git"
" Bundle "tpope/vim-rails.git"
" Bundle "ecomba/vim-ruby-refactoring"
" Bundle "vim-scripts/matchit.zip.git"
" Bundle "tpope/vim-endwise.git"
" Bundle "skwp/vim-html-escape"
" Bundle "Shougo/neocomplcache.git"
" Bundle "tpope/vim-fugitive"
" Bundle "skwp/vim-git-grep-rails-partial"
" Bundle "tpope/vim-unimpaired"
" Bundle "vim-scripts/lastpos.vim"
" Bundle "sjl/gundo.vim"
Bundle "vim-scripts/sudo.vim"
" Bundle "mileszs/ack.vim"
" Bundle "nelstrom/vim-textobj-rubyblock"
" Bundle "kana/vim-textobj-user"
" Bundle "austintaylor/vim-indentobject"
" Bundle "kana/vim-textobj-datetime"
" Bundle "kana/vim-textobj-entire"
" Bundle "mattn/gist-vim"
" Bundle "godlygeek/tabular"
" Bundle "AndrewRadev/splitjoin.vim"
" Bundle "vim-scripts/argtextobj.vim"
" Bundle "bootleq/vim-textobj-rubysymbol"
" Bundle "nathanaelkane/vim-indent-guides"
" Bundle "tpope/vim-haml"
" Bundle "claco/jasmine.vim"
" Bundle "kana/vim-textobj-function"
" Bundle "kchmck/vim-coffee-script"
" Bundle "wavded/vim-stylus"
" Bundle "vim-scripts/Vim-R-plugin"
" Bundle "kien/ctrlp.vim"
" Bundle "majutsushi/tagbar.git"
" Bundle "chrisbra/color_highlight.git"
" Bundle "vim-scripts/camelcasemotion.git"
" Bundle "garbas/vim-snipmate.git"
" Bundle "MarcWeber/vim-addon-mw-utils.git"
" Bundle "tomtom/tlib_vim.git"
" Bundle "honza/vim-snippets.git"
" Bundle "skwp/vim-conque"
" Bundle "gregsexton/gitv"
" Bundle "briandoll/change-inside-surroundings.vim.git"
" Bundle "timcharper/textile.vim.git"
" Bundle "vim-scripts/Specky.git"
" Bundle "tpope/vim-bundler"
" Bundle "tpope/vim-rake.git"
" Bundle "skwp/vim-easymotion"
" Bundle "groenewege/vim-less.git"
" Bundle "mattn/webapi-vim.git"
" Bundle "astashov/vim-ruby-debugger"
" Bundle "aaronjensen/vim-sass-status.git"
" Bundle "skwp/vim-powerline.git"
" Bundle "briancollins/vim-jst"
" Bundle "pangloss/vim-javascript"
" Bundle "tpope/vim-abolish"
" Bundle "jtratner/vim-flavored-markdown.git"
" Bundle "xsunsmile/showmarks.git"
" Bundle "digitaltoad/vim-jade.git"
" Bundle "tpope/vim-ragtag"
" Bundle "vim-scripts/TagHighlight.git"
" Bundle "itspriddle/vim-jquery.git"
" Bundle "slim-template/vim-slim.git"
" Bundle "airblade/vim-gitgutter.git"
" Bundle "bogado/file-line.git"
" Bundle "tpope/vim-rvm.git"
" Bundle "nelstrom/vim-visual-star-search"
" Bundle "myusuf3/numbers.vim.git"



" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

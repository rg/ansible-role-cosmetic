rgarrigue.cosmetic
==================


Cosmetic stuff, mostly settings I don't want in `rgarrigue.common` since I won't impose it to other people like my own `vimrc` [![Build Status](https://travis-ci.org/rgarrigue/ansible-role-cosmetic.svg?branch=master)](https://travis-ci.org/rgarrigue/ansible-role-cosmetic)

Requirements
------------

Developped and tested on *Ubuntu Server 16.10 Yakkety*, and *CentOS 6 & 7*

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: all
      roles:
        - role: rgarrigue.cosmetic

License
-------

GPLv3

Author Information
------------------

Rémy Garrigue
